<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'ieconfigplus_titre' => 'Importeur/Exporteur de configuration PLUS',
	// I
  'ieconfig_attention_meme_identifiant' => 'Attention il existe d&eacute;ja un &eacute;l&eacute;ment portant cet identifiant',
  'ieconfig_importer'	=> 'Importer',
  'ieconfig_ne_pas_importer'	=> 'Ne pas importer',
  'ieconfig_renommer'	=> 'Renommer (titre_%date)',
  'ieconfig_remplacer' => 'Remplacer',

	// F
	'formidable_export_titre'	=> 'Export des formulaires',
	'formidable_choix_export'	=> 'Choix des formulaires &agrave; exporter',

	// M
	'mots_export_titre' => 'Exporter des mots-clefs',
	'mots_choix_export' => 'Choix des groupes et mots &agrave; exporter',
	'mots_import_titre' => 'Importation des Mots-clef',
	'mots_choix_import' => 'Choix des mots à importer',
	// P
  'pages_export_titre'	=> 'Export des Pages',
  'pages_choix_export'	=> 'Choix des pages &agrave; exporter',
  'pages_import_titre'	=> 'Import des Pages',
  'pages_choix_import'    => 'Choix des pages &agrave; importer',

	// S
	'selections_editoriales_export_titre'	=> 'Export des sélections éditoriales',
	'selections_editoriales_choix_export'	=> 'Choix des s&eacute;lections &agrave; exporter'

);
